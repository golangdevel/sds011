module gitlab.eph28.com/golang/sds011

go 1.14

require (
	github.com/eclipse/paho.mqtt.golang v1.2.0
	github.com/tarm/serial v0.0.0-20180830185346-98f6abe2eb07
	gitlab.com/mjwhitta/cli v1.7.9
	gitlab.com/mjwhitta/jsoncfg v1.4.2
	gitlab.com/mjwhitta/pathname v1.0.6
	golang.org/x/net v0.0.0-20200425230154-ff2c4b7c35a0 // indirect
	golang.org/x/sys v0.0.0-20200430202703-d923437fa56d // indirect
)
