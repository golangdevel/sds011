This program was written to collect data from the SDS011 PM Nova PM Sensor Laser
PM2.5 Air Quality Detection Sensor Module Dust Sensor Air Conditioning Monitor
Module. I use this app to run on a Raspberry PI and collect sample Particulate Matter data
and feed the data to Home Assistant to see in Grafana graphs.
It will collect numSamples and then report the highest amount to stdout, 
a JSON text file, or an MQTT server.

*** IMPORTANT ***
If you are already using a collection program, when sds011 is run, the
device may be left in sleep and query mode. To set the device back to the
settings it was shipped from the factory, run with -F option to reset
device back to factory defaults. 

To build for different architechure or OS, use GOARCH and GOOS to build,

For example, the following command will make a binary for an ARM system like
a Raspberry PI

GOARCH=arm make

or

GOOS=windows make

--------------------------------------------------------------------------------
Usage: build/linux/amd64/sds011 [OPTIONS]

DESCRIPTION
    App to read SDS011 particulate matter sensor

OPTIONS
    -c, --config=STRING     Config filename
    -D, --debug             Enable debugging
    -d, --display           Don't Display All Samples
    -F, --factory           Set SDS011 back to factory settings
    -f, --firmware          Show Firmware version
    -h, --help              Display this help message.
    -n, --numsamples=INT    Number of samples to run between sleeps (30)
    -p, --port=STRING       Port to open [/dev/ttyUSB0]
    -Q, --query             Set Query mode
    -q, --quiet             Set Quiet mode. No Output
    -S, --Sleep             Put Sensor to Sleep
    -s, --sleeptime=INT     Amount to sleep (120)
    -w, --wakeup            Wakeup device and exit

--------------------------------------------------------------------------------
The sds011.conf file contains default values and can be over written by the 
command line options. It will be created in the current working directory
unless it already exists or '-c' is given. For example, '-c /tmp/sds011.conf'
will use/create the file /tmp/sds011.conf

sds011.conf
{
  "devName": "/dev/ttyUSB0",
  "jsonFile": "",
  "jsonMax": 100,
  "mqttHost": "",
  "mqttPasswd": "",
  "mqttPort": 8883,
  "mqttSSL": true,
  "mqttTopic": "sds011/room",
  "mqttUserid": "",
  "numEntries": 100,
  "numSamples": 30,
  "sleepTime": 120
}

--------------------------------------------------------------------------------
sds011 -f
Firmware date 2018-11-16 ID: 0xFEFE
  1) PM 2.5: 0.0 μg/m^3 AQI: 0	PM 10: 0.0 μg/m^3 AQI: 0
  2) PM 2.5: 0.0 μg/m^3 AQI: 0	PM 10: 0.0 μg/m^3 AQI: 0


--------------------------------------------------------------------------------
Thanks to:
https://openschoolsolutions.org/measure-particulate-matter-with-a-raspberry-pi/
DATASHEET - http://cl.ly/ekot
Nice case -  https://www.thingiverse.com/thing:2219718
