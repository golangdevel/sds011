package main

import (
	"gitlab.com/mjwhitta/jsoncfg"
	"gitlab.com/mjwhitta/pathname"
	"path/filepath"
)

var config *jsoncfg.JSONCfg

func init() {
	// Initialize default values for config
	config = jsoncfg.New("sds011.config")
	config.SetDefault("/dev/ttyUSB0", "devName")
	config.SetDefault("", "mqttHost")
	config.SetDefault("", "mqttPasswd")
	config.SetDefault(8883, "mqttPort")
	config.SetDefault(true, "mqttSSL")
	config.SetDefault("sds011/room", "mqttTopic")
	config.SetDefault("", "mqttUserid")
	config.SetDefault("", "jsonFile")
	config.SetDefault(100, "numEntries")
	config.SetDefault(120, "sleepTime")
	config.SetDefault(30, "numSamples")
	config.SetDefault(100, "jsonMax")
	//config.SaveDefault()
	//config.Reset()
}

// setName sets the config location and filename
func setName(fileName string) {
	config.File = pathname.ExpandPath(filepath.Join(fileName))
	config.SaveDefault()
	config.Reset()
}
