package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"time"
)

type jsonMqtt struct {
	Pm10 float64 `json:"pm10"`
	Pm25 float64 `json:"pm25"`
	Time string  `json:"time"`
}

func jsonSave(jsonfile string, jsonmax int, pm25, pm10 float64) {
	var js []jsonMqtt
	var njs jsonMqtt

	if jsonmax < 1 {
		jsonmax = 100
	}
	data, err := ioutil.ReadFile(jsonfile)
	if err == nil {
		err = json.Unmarshal(data, &js)
		ljs := len(js)
		if ljs > jsonmax-1 {
			js = js[ljs-jsonmax+1 : ljs]
		}
	} else {
		fmt.Print(err)
	}
	njs.Pm25 = pm25
	njs.Pm10 = pm10
	njs.Time = time.Now().Format("02.01.2006 15:04:05")
	js = append(js, njs)
	file, _ := json.MarshalIndent(js, "", " ")
	_ = ioutil.WriteFile(jsonfile, file, 0644)

}
