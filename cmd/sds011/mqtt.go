package main

import (
	"crypto/tls"
	"fmt"
	"os"
	"time"

	MQTT "github.com/eclipse/paho.mqtt.golang"
)

// MQTTCfg structure to hold MQTT data
type MQTTCfg struct {
	hostname string
	port     int
	username string
	password string
	topic    string
	message  string
	ssl      bool
}

func (c MQTTCfg) mqttSent() {
	var server string

	hostname, _ := os.Hostname()

	qos := 0
	retained := false
	clientid := fmt.Sprintf("%s.%d", hostname, time.Now().Second())

	if c.ssl {
		server = fmt.Sprintf("ssl://%s:%d", c.hostname, c.port)
	} else {
		server = fmt.Sprintf("tcp://%s:%d", c.hostname, c.port)
	}
	connOpts := MQTT.NewClientOptions().AddBroker(server).SetClientID(clientid).SetCleanSession(true)
	if c.username != "" {
		connOpts.SetUsername(c.username)
		if c.password != "" {
			connOpts.SetPassword(c.password)
		}
	}
	tlsConfig := &tls.Config{InsecureSkipVerify: true, ClientAuth: tls.NoClientCert}
	connOpts.SetTLSConfig(tlsConfig)

	client := MQTT.NewClient(connOpts)
	if token := client.Connect(); token.Wait() && token.Error() != nil {
		fmt.Println(token.Error())
		return
	}
	// fmt.Printf("Connected to %s\n", server)
	if token := client.Publish(c.topic, byte(qos), retained, c.message); token.Wait() && token.Error() != nil {
		fmt.Println(token.Error())
	}
	client.Disconnect(100)

}
