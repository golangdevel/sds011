package main

import (
	"fmt"
	"github.com/tarm/serial"
	"gitlab.com/mjwhitta/cli"
	"log"
	"os"
	"time"
)

// Version is the package version.
const Version = "0.2"

var (
	// go doc --all github.com/tarm/serial
	port        *serial.Port
	_debug      = false
	_devName    string
	_putAsleep  = false
	_wakeUP     = false
	_display    = false
	_showfw     = false
	_setFactory = false
	_setQuery   = false
	_sleepTime  = 60
	_numSamples = 15
	_quiet      = false
	_configFile string
	aqi         = []float64{0, 50, 100, 150, 200, 300, 400, 500}
)

const (
	_sleep = 0
	_work  = 1

	cmdMode          = 2
	cmdQueryData     = 4
	cmdDeviceID      = 5
	cmdSleep         = 6
	cmdFirmware      = 7
	cmdWorkingPeriod = 8

	modeActive = 0
	modeQuery  = 1

	periodContinuous = 0

	_head   = 0
	_cmdid  = 1
	_datab  = 2
	_db14   = 15
	_db15   = 16
	_chksum = 17
	_tail   = 18
)

func calcAQIpm25(pm25 float64) int {
	apm := [8]float64{0, 12, 35.4, 55.4, 150.4, 250.4, 350.4, 500.4}

	aqipm25 := 0.0
	for ix := 0; ix < len(apm)-1; ix++ {
		if (pm25 >= apm[ix]) && (pm25 <= apm[ix+1]) {
			aqipm25 = ((aqi[ix+1]-aqi[ix])/(apm[ix+1]-apm[ix]))*(pm25-apm[ix]) + aqi[ix]
		}
	}
	return int(aqipm25)
}

func calcAQIpm10(pm10 float64) int {
	apm := [8]float64{0, 54, 154, 254, 354, 424, 504, 604}

	aqipm10 := 0.0
	for ix := 0; ix < len(apm)-1; ix++ {
		if (pm10 >= apm[ix]) && (pm10 <= apm[ix+1]) {
			aqipm10 = ((aqi[ix+1]-aqi[ix])/(apm[ix+1]-apm[ix]))*(pm10-apm[ix]) + aqi[ix]
		}
	}
	return int(aqipm10)
}

/******************************************************************************
 *
 ******************************************************************************/
func constructCommand(data []byte) []byte {
	var ix int
	var retb []byte

	retb = make([]byte, 19, 19)

	retb[_head] = 0xaa
	retb[_cmdid] = 0xb4
	retb[_db14] = 0xff
	retb[_db15] = 0xff
	retb[_tail] = 0xab
	retb[_datab] = data[0]

	chksum := int(data[0])
	chksum += int(retb[_db14])
	chksum += int(retb[_db15])
	for ix = 1; ix < len(data); ix++ {
		chksum += int(data[ix])
		retb[_datab+ix] = data[ix]
	}
	chksum = chksum % 256
	retb[_chksum] = byte(chksum)
	return retb
}

/******************************************************************************
 *
 ******************************************************************************/
func readResponse(datab1 byte) []byte {

	count := 0
	buf := make([]byte, 9)
	buf1 := make([]byte, 1)
	for {
		ix, err := port.Read(buf1)
		if ix == 0 {
			if !_quiet {
				fmt.Printf("Read Response OOps\n")
			}
			break
		}
		if err != nil {
			log.Fatalf("port.Read: %v", err)
		}
		if buf1[0] == 0xaa {
			port.Read(buf)
			if buf[0] == 0xc0 {
				if datab1 != cmdQueryData {
					if _debug {
						fmt.Printf("RESPONSE: Got Query Data. Discard\n\t")
						for ix := 0; ix < len(buf); ix++ {
							fmt.Printf("%02X ", buf[ix])
						}
						fmt.Printf("\n")
					}
					count++
					if count > 3 {
						break
					}
					continue
				}
			} else if buf[1] != datab1 {
				if _debug {
					fmt.Printf("RESPONSE: Wrong response: Wanted %02X got %02X\n",
						datab1, buf[1])
					for ix := 0; ix < len(buf); ix++ {
						fmt.Printf("%02X ", buf[ix])
					}
					fmt.Printf("\n")
				}
				continue
			}
			break
		}
	}
	ret := append(buf1, buf...)
	if _debug {
		fmt.Printf("RESPONSE: ")
		for ix := 0; ix < len(ret); ix++ {
			fmt.Printf("%02X ", ret[ix])
		}
		fmt.Printf("\n")
	}
	return ret
}

/******************************************************************************
 *
 ******************************************************************************/

func firmwareVer() (string, string) {
	var query = []byte{cmdFirmware} // Get firmware version
	var fver string
	var id string

	data := constructCommand(query)
	if _debug {
		fmt.Printf("FIRMWARE: ")
		for ix := 0; ix < len(data); ix++ {
			fmt.Printf("%02X ", data[ix])
		}
		fmt.Printf("\n")
	}

	_, err := port.Write(data)
	if err != nil {
		log.Fatalf("port.Read: %v", err)
	}
	buf := readResponse(cmdFirmware)

	if buf[1] == 0xC5 {
		if _showfw {
			if !_quiet {
				fmt.Printf("Firmware date 20%02d-%02d-%02d ID: 0x%02X%02X\n",
					buf[3], buf[4], buf[5], buf[7], buf[6])
			}
		}
		id = fmt.Sprintf("0x%02X%02X", buf[7], buf[6])
		fver = fmt.Sprintf("20%02d-%02d-%02d", buf[3], buf[4], buf[5])
	}
	return id, fver
}

/******************************************************************************
 *
 ******************************************************************************/

func setWorkingPeriod(period byte) {
	var query = []byte{cmdWorkingPeriod, 1, period} // Set Working period

	data := constructCommand(query)
	if _debug {
		fmt.Printf("PERIOD  : ")
		for ix := 0; ix < len(data); ix++ {
			fmt.Printf("%02X ", data[ix])
		}
		fmt.Printf("\n")
	}

	_, err := port.Write(data)
	if err != nil {
		log.Fatalf("port.Read: %v", err)
	}
	readResponse(cmdWorkingPeriod)

}

/******************************************************************************
 *
 ******************************************************************************/

func pollDevice() (int, int) {
	var query = []byte{cmdQueryData} // Send Query Data command

	data := constructCommand(query)
	if _debug {
		fmt.Printf("QUERY   : ")
		for ix := 0; ix < len(data); ix++ {
			fmt.Printf("%02X ", data[ix])
		}
		fmt.Printf("\n")
	}

	_, err := port.Write(data)
	if err != nil {
		log.Fatalf("port.Read: %v", err)
	}
	buf := readResponse(cmdQueryData)
	pm25 := 0
	pm10 := 0

	if buf[1] == 0xC0 {
		pm25 = int(buf[3]) << 8
		pm25 += int(buf[2])

		pm10 = int(buf[5]) << 8
		pm10 += int(buf[4])
	}

	return pm25, pm10
}

/******************************************************************************
 *
 ******************************************************************************/

func cmdSetMode(mode byte) {
	var setmode = []byte{cmdMode, 1, mode} // Set Query/Active Mode

	data := constructCommand(setmode)
	if _debug {
		fmt.Printf("Set MODE: ")
		for ix := 0; ix < len(data); ix++ {
			fmt.Printf("%02X ", data[ix])
		}
		fmt.Printf("\n")
	}

	_, err := port.Write(data)
	if err != nil {
		log.Fatalf("port.Read: %v", err)
	}
	readResponse(cmdMode)
}

/******************************************************************************
 *
 ******************************************************************************/

func cmdSetSleep(mode byte) {
	var sleep = []byte{cmdSleep, 1, mode} // Set Work/Sleep Mode
	var data []byte

	data = constructCommand(sleep)
	if _debug {
		if mode == 1 {
			fmt.Printf("WAKEUP:   ")
		} else {
			fmt.Printf("SLEEP:    ")
		}
		for ix := 0; ix < len(data); ix++ {
			fmt.Printf("%02X ", data[ix])
		}
		fmt.Printf("\n")
	}

	_, err := port.Write(data)
	if err != nil {
		log.Fatalf("port.Read: %v", err)
	}
	if mode == _work { // Allow a little spin up time
		time.Sleep(2 * time.Second)
	}
	readResponse(cmdSleep)
}

/******************************************************************************
 *
 ******************************************************************************/
func main() {
	var err error

	var mq MQTTCfg

	setName(_configFile)

	mq.hostname = config.GetString("mqttHost")
	mq.port = config.GetInt("mqttPort")
	mq.username = config.GetString("mqttUserid")
	mq.password = config.GetString("mqttPasswd")
	mq.topic = config.GetString("mqttTopic")
	mq.ssl = config.GetBool("mqttSSL")
	devName := config.GetString("devName")
	jsonFile := config.GetString("jsonFile")
	jsonMax := config.GetInt("jsonMax")

	if _devName == "" {
		_devName = devName
	}
	if _sleepTime == 0 {
		_sleepTime = config.GetInt("sleepTime")
	}
	if _numSamples == 0 {
		_numSamples = config.GetInt("numSamples")
	}
	if mq.hostname != "" {
		if !_quiet {
			fmt.Printf("mqtt host = %s\n", mq.hostname)
		}
	}
	c := &serial.Config{Name: _devName, Baud: 9600,
		ReadTimeout: time.Second * 4}

	port, err = serial.OpenPort(c)
	if err != nil {
		log.Fatalf("serial.Open: %v", err)
	}

	// Make sure to close it later.
	defer port.Close()

	cmdSetSleep(_work) // Wake up

	if _setFactory {
		cmdSetMode(modeActive) // set query mode
		os.Exit(0)
	}
	if _setQuery {
		cmdSetMode(modeQuery) // set query mode
		cmdSetSleep(_sleep)   // set device to sleep mode
		os.Exit(0)
	}
	if _putAsleep {
		cmdSetSleep(_sleep) // go to sleep
		os.Exit(0)
	}
	if _wakeUP {
		cmdSetSleep(_work) // Wake up
		os.Exit(0)
	}

	// id, fver := firmwareVer()
	id, fver := firmwareVer()

	if !_quiet {
		fmt.Printf("ID = %s date = %s\n", id, fver)
	}
	cmdSetMode(modeQuery) // set query mode
	setWorkingPeriod(periodContinuous)

	for {
		mpm25 := 0
		mpm10 := 0
		for ix := 0; ix < _numSamples; ix++ {
			pm25, pm10 := pollDevice()
			if _display {
				pm25f := float64(pm25) / 10.0
				pm10f := float64(pm10) / 10.0
				if !_quiet {
					fmt.Printf("%3d) PM 2.5: %.1f μg/m^3 AQI: %d\tPM 10: %.1f μg/m^3 AQI: %d\n",
						ix+1, pm25f, calcAQIpm25(pm25f), pm10f, calcAQIpm10(pm10f))
				}
			}
			time.Sleep(1 * time.Second)
			if pm25 > mpm25 {
				mpm25 = pm25
			}
			if pm10 > mpm10 {
				mpm10 = pm10
			}
		}
		pm25f := float64(mpm25) / 10.0
		pm10f := float64(mpm10) / 10.0
		curTime := time.Now().Format(time.Stamp)
		if jsonFile != "" {
			jsonSave(jsonFile, jsonMax, pm25f, pm10f)
		}
		if mq.hostname != "" {
			mq.message = fmt.Sprintf("{ \"ts\": \"%s\", \"id\""+
				":\"%s\", \"firmware\":\"%s\", \"pm25\": %.1f, "+
				"\"pm25_aqi\": %d, \"pm10\": %.1f, \"pm10_aqi\": %d }",
				curTime, id, fver, pm25f, calcAQIpm25(pm25f),
				pm10f, calcAQIpm10(pm10f))
			mq.mqttSent()
		}
		if !_quiet {
			fmt.Printf("\n%s PM 2.5: %.1f μg/m^3 AQI: %d\tPM 10: %.1f μg/m^3 AQI: %d\n\n",
				curTime, pm25f, calcAQIpm25(pm25f), pm10f, calcAQIpm10(pm10f))
		}

		cmdSetSleep(_sleep) // go to sleep

		if !_quiet {
			fmt.Printf("Sleeping for a %d seconds\n", _sleepTime)
		}
		time.Sleep(time.Duration(_sleepTime) * time.Second)
		cmdSetSleep(_work) // Wake up
	}
}

func init() {
	// Configure cli package
	cli.Align = true
	cli.Info = "App to read SDS011 particulate matter sensor"
	cli.Flag(&_debug, "D", "debug", false, "Enable debugging")
	cli.Flag(&_configFile, "c", "config", "sds011.conf", "Config filename")
	cli.Flag(&_display, "d", "display", true, "Don't Display All Samples")
	cli.Flag(&_devName, "p", "port", "", "Port to open [/dev/ttyUSB0]")
	cli.Flag(&_putAsleep, "S", "Sleep", false, "Put Sensor to Sleep")
	cli.Flag(&_sleepTime, "s", "sleeptime", 0, "Amount to sleep (120)")
	cli.Flag(&_numSamples, "n", "numsamples", 0, "Number of samples to run between sleeps (30)")
	cli.Flag(&_wakeUP, "w", "wakeup", false, "Wakeup device and exit")
	cli.Flag(&_showfw, "f", "firmware", false, "Show Firmware version")
	cli.Flag(&_setFactory, "F", "factory", false, "Set SDS011 back to factory settings")
	cli.Flag(&_setQuery, "Q", "query", false, "Set Query mode")
	cli.Flag(&_quiet, "q", "quiet", false, "Set Quiet mode. No Output")
	cli.Parse()
}
